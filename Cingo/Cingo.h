//
//  Cingo.h
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <UIKit/UIKit.h>

#include "CingoSession.h"
#include "CingoCustomer.h"
#include "CingoRequest.h"
#include "CingoVendor.h"


//! Project version number for Cingo.
FOUNDATION_EXPORT double CingoVersionNumber;

//! Project version string for Cingo.
FOUNDATION_EXPORT const unsigned char CingoVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Cingo/PublicHeader.h>

static NSString* const kApiKey = @"45419262";
// Replace with your generated session ID
static NSString* const kSessionId = @"2_MX40NTQxOTI2Mn5-MTQ2NDI3MDUzNDI2NX5vMHJsZEFqTCsxYVlsUkpFTWlkM1RiWDJ-fg";
// Replace with your generated token
static NSString* const kToken = @"T1==cGFydG5lcl9pZD00NTQxOTI2MiZzaWc9NmZlYWJjMmJlNTViYzc4YTMwMmM1ZmQwMmIyMGMzYmM5YjFhZGYzZDpzZXNzaW9uX2lkPTJfTVg0ME5UUXhPVEkyTW41LU1UUTJOREkzTURVek5ESTJOWDV2TUhKc1pFRnFUQ3N4WVZsc1VrcEZUV2xrTTFSaVdESi1mZyZjcmVhdGVfdGltZT0xNDY0MjcwNTQ1Jm5vbmNlPTAuNDY4MDcxODU1MzcxODE3OTUmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTQ2NDI3NDE0NA==";




#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif

// ALog will always output like NSLog

#define ALog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

// ULog will show the UIAlertView only when the DEBUG variable is set

#ifdef DEBUG
#   define ULog(fmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n [Line %d] ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:fmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }
#else
#   define ULog(...)
#endif
