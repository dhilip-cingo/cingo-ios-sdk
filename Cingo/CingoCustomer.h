//
//  Customer.h
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CingoCustomer : NSObject
@property (nonatomic,strong) NSString *email, *password, *firstName, *lastName, *mobileNumber,*customerId, *street,*zipcode,*city,*state;
- (NSString*)fullName;
- (NSDictionary*)jsonObject;
+ (CingoCustomer*)jsonToObject:(NSDictionary*)json;
@end
/*
 customer" :
 {
 "email" : "abc@gmail.com",
 "password" : "test1234",
 "password_confirmation" : "test1234",
 "name": "test",
 "mobile_number" : "512.364.1309"
 }
 }
*/