//
//  Customer.m
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "CingoCustomer.h"

@implementation CingoCustomer

- (NSDictionary*)jsonObject{
  [self guardForNilCrash];
  return @{
           @"customer" : @{
           @"email":self.email,
           @"password":self.password,
           @"password_confirmation":self.password,
           @"name":[self fullName],
           @"mobile_number":self.mobileNumber,
           @"customerId":self.customerId,
           @"state":self.state,
           @"street":self.street,
           @"city":self.city,
           @"zip":self.zipcode
           }
           };
}

- (void)guardForNilCrash{
  if (self.customerId == nil) {
    self.customerId = @"";
  }
  if (self.state == nil){
    self.state = @"";
  }
  if (self.street == nil){
    self.street = @"";
  }
  if (self.city == nil){
    self.city = @"";
  }
  if (self.zipcode == nil){
    self.zipcode = @"";
  }
  if(self.firstName == nil && self.lastName == nil){
    self.firstName = @"";
    self.lastName = @"";
  }
  if (self.mobileNumber==nil){
    self.mobileNumber=@"";
  }
}
- (NSString*)fullName{
  return [NSString stringWithFormat:@"%@ %@",self.firstName,self.lastName];
}

+ (CingoCustomer*)jsonToObject:(NSDictionary*)json{
  CingoCustomer *customer = [[CingoCustomer alloc]init];
  json = [json objectForKey:@"customer"];
  [customer setEmail:json[@"email"]];
  [customer setLastName:@""];
  [customer setFirstName:json[@"name"]];
  [customer setPassword:json[@"password"]];
  [customer setMobileNumber:json[@"mobile_number"]];
  [customer setCity:json[@"city"]];
  [customer setState:json[@"state"]];
  [customer setZipcode:json[@"zip"]];
  [customer setStreet:json[@"street"]];
  [customer setCustomerId:json[@"customerId"]];
  return customer;
}



/*
 customer" :
 {
 "email" : "abc@gmail.com",
 "password" : "test1234",
 "password_confirmation" : "test1234",
 "name": "test",
 "mobile_number" : "512.364.1309"
 }
 }
 */
@end
