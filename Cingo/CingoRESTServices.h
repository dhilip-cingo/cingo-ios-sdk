//
//  RESTServices.h
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^WebServicesResponse)(NSURLResponse *response, id responseObject, NSError *error);
@interface CingoRESTServices : NSObject
- (void)dataRequest:(NSURLRequest*)request callback:(WebServicesResponse)webServiceResponse;
@end
