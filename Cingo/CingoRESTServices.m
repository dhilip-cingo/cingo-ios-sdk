//
//  RESTServices.m
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "CingoRESTServices.h"
#import "Cingo.h"
#import <AFNetworking/AFNetworking.h>

@implementation CingoRESTServices
- (void)dataRequest:(NSURLRequest*)request callback:(WebServicesResponse)webServiceResponse{
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *session = [NSURLSession sharedSession];
  AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
  AFSecurityPolicy* policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
  [policy setValidatesDomainName:NO];
  [policy setAllowInvalidCertificates:YES];
  [policy setValidatesDomainName:NO];
  [manager setSecurityPolicy:policy];
  [manager setResponseSerializer:[AFJSONResponseSerializer  serializerWithReadingOptions:NSJSONWritingPrettyPrinted ]];
  DLog(@"Web Services Request: %@", request.debugDescription);
  NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error){
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
    DLog(@"Response for request - %@",request.URL.absoluteString);
    DLog(@"Webservices status %u",httpResponse.statusCode );
    if (error) {
      DLog(@"Web Services Error: %@", error.localizedDescription);
      DLog(@"Web Services Error Response: %@", responseObject);
    } else {
      DLog(@"Web Services Response - %@", responseObject);
    }
    webServiceResponse(response,responseObject,error);
  }];
  [dataTask resume];
}

@end
