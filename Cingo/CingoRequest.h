//
//  CingoRequest.h
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CingoRequest : NSObject
@property (nonatomic,strong) NSString *customerId, *requestTime, *requestDescription, *departmentId,*vendorId,*attachmentDetails, *requestId;
@property (nonatomic,strong) NSString *notes;
- (NSDictionary*)jsonObject;
- (NSDictionary*)notesJsonObject;
@end
