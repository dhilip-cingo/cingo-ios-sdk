//
//  CingoRequest.m
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "CingoRequest.h"

@implementation CingoRequest
- (NSDictionary*)jsonObject{
 return @{@"user_request" :
    @{
    @"customer_id": self.customerId,
    @"request_time": self.requestTime,
    @"description": self.requestDescription,
    @"department_id": self.departmentId,
    @"vendor_id" : self.vendorId,
    @"attachments_attributes": @{
 
    }
    }
          };
}

- (NSDictionary*)notesJsonObject{
  return @{
    @"note" :
    @{
      @"description": self.notes,
      @"added_by_id": self.customerId,
      @"added_by_type": @"Customer"
    }
  };
}
@end
