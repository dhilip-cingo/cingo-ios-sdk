//
//  CingoSession.h
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cingo.h"
#import "CingoCustomer.h"
#import "CingoVendor.h"
#import "CingoRESTServices.h"
@class CingoCustomer;
@class CingoRequest;
@class OTSubscriberKit;
typedef void (^CustomerWebServicesResponse)(CingoCustomer *responseObject, NSError *error);
typedef void (^RequestWebServicesResponse)(CingoRequest *request, NSError *error);
@protocol CingoSessionDelegate <NSObject>

- (void)streamAvailableForRendering:(UIView*)subscriberView;
- (void)sessionCallInitiated;

@end
@interface CingoSession : NSObject
@property (nonatomic,strong)CingoCustomer * customer;
@property (nonatomic,strong)CingoVendor *vendor;
@property (nonatomic,strong)NSString *authToken, *sessionId,*token,*customerId;
@property (nonatomic,strong)NSString *currentReqeustId;
@property (nonatomic,weak) id<CingoSessionDelegate> delegate;
+(CingoSession*)sharedCingoSession;

- (void)initializeSession;
- (BOOL)isInternetConnected;
- (void)startWebrtcSession;

- (void)signInCustomer:(NSString*)email password:(NSString*)password callback:(CustomerWebServicesResponse)webServicesResponse;

- (void)signUpNewCustomer:(NSString*)email password:(NSString*)password firstName:(NSString*)firstName lastName:(NSString*)lastName mobile:(NSString*)mobileNumber;
- (void)signUpNewCustomer:(CingoCustomer*)customer callback:(CustomerWebServicesResponse)callback;

- (void)changeCustomerSettings:(NSString*)email password:(NSString*)password firstName:(NSString*)firstName lastName:(NSString*)lastName mobile:(NSString*)mobileNumber;
- (void)changeCustomerSettings:(CingoCustomer*)customer callback:(CustomerWebServicesResponse)webServicesResponse;

- (void)fetchAllUserRequests:(WebServicesResponse)webServicesResponse;
- (void)fetchRequestFor:(NSString*)requestId callback:(WebServicesResponse)webServicesResponse;
//- (void)createNewRequest:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse;

- (void)createNewRequest:(CingoRequest*)request callback:(RequestWebServicesResponse)webServicesResponse;

- (void)changeRequestInformation:(NSString*)requestId  json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse;
- (void)postCustomerRatingFor:(NSString*)requestId json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse;
- (void)addRequestNotesFor:(NSString*)requestId json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse;

- (void)dial;
- (void)endCall;
- (void)sendSignalMessage:(NSString*)message;
- (NSString*)expectedCallSignalString;
- (NSString*)expectedAcceptCallSignalString;
@end

