//
//  CingoSession.m
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "CingoSession.h"
#import "Cingo.h"
#import "CingoWebServices.h"
#import <AFNetworking/AFNetworking.h>
#import "CingoWebrtcSession.h"
#import "CingoUtils.h"

static CingoSession *cingoSession = nil;
@interface CingoSession(){
  CingoSession *weakSelf;
}
@property (nonatomic,strong)CingoWebServices *cingoWebServices;
@property (nonatomic,assign)BOOL internetConnected;
@property (nonatomic,assign)BOOL cameraPermission, micPermission;
@property (nonatomic,strong)CingoWebrtcSession *webrtcSession;

@end

@implementation CingoSession

+(CingoSession*)sharedCingoSession{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    cingoSession = [[CingoSession alloc]init];
    [cingoSession initializeSession];
  });
  return cingoSession;
}
- (void)initializeSession{
  self.cingoWebServices = [[CingoWebServices alloc]init];
  self.cingoWebServices.cingoSession = self;
  [self.cingoWebServices initializeWebServicesSession];

  [self loadSavedCustomer];
  weakSelf = self;
 [[AFNetworkReachabilityManager sharedManager] startMonitoring];
  __weak CingoSession *weakSelf = self;
  [[AFNetworkReachabilityManager sharedManager]setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
    
    if (status == AFNetworkReachabilityStatusUnknown ||status == AFNetworkReachabilityStatusNotReachable ){
      DLog(@"Internet Connectivity Not Available");
    }
    
    else if  (status == AFNetworkReachabilityStatusReachableViaWWAN ||status == AFNetworkReachabilityStatusReachableViaWiFi ){
      weakSelf.internetConnected = true;
      DLog(@"Internet Connectivity Available");
    }
    
  }];
  
  
}

- (BOOL)isInternetConnected{
  return [AFNetworkReachabilityManager sharedManager].reachable;
}

- (void)startWebrtcSession{
  self.webrtcSession = [[CingoWebrtcSession alloc]init];
  [self.webrtcSession initializeSession];
}

- (void)dial{
  [self.webrtcSession dial];
}

- (void)endCall{
  [self.webrtcSession endCall];
}
- (void)sendSignalMessage:(NSString*)type message:(NSString*)message{
  [self.webrtcSession sendSignalMessage:type message:message];
}
- (BOOL)checkForPreRequisites{
  dispatch_group_t group = dispatch_group_create();
  dispatch_queue_t _myQueue = dispatch_queue_create("com.cingo.cingo",
                                                    0);
  __weak CingoSession *weakSelf =self;
    dispatch_group_async(group, _myQueue, ^{
      //  do some long running task.
      [weakSelf checkForCameraPermission];
    });
  dispatch_group_async(group, _myQueue, ^{
    //  do some long running task.
    [weakSelf checkForMicPermission];
  });
  dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
  if (!self.cameraPermission){
    [CingoUtils showAlertView:@"" message:@"Camera Permission is required For Cingo" button1:nil button1Action:nil otherButton:@"Close"];
    return false;
  }
  
  if (!self.micPermission){
    [CingoUtils showAlertView:@"" message:@"Mic Permission is required For Cingo" button1:nil button1Action:nil otherButton:@"Close"];
    return false;
  }
  
  if(!self.isInternetConnected){
    [CingoUtils showAlertView:@"" message:@"Mic Permission is required For Cingo" button1:nil button1Action:nil otherButton:@"Close"];
    return false;
  }
  
  return true;

}

- (void)checkForCameraPermission{
  __weak CingoSession *weakSelf =self;
  [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
    weakSelf.cameraPermission = granted;

  }];
}

- (void)checkForMicPermission{
  __weak CingoSession *weakSelf =self;
  [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
    weakSelf.micPermission = granted;
  }];
}

- (void)signInCustomer:(NSString*)email password:(NSString*)password callback:(CustomerWebServicesResponse)webServicesResponse{
  NSParameterAssert(email);
  NSParameterAssert(password);

  NSDictionary *json =@{@"customer" :
                          @{
                            @"email": email,
                            @"password": password
                            }
                        };
  [self.cingoWebServices signInCustomer:json
                               callback:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 
                                 if (!error){
                                   if (responseObject){
                                     if(weakSelf.customer == nil){
                                       weakSelf.customer = [[CingoCustomer alloc]init];
                                       weakSelf.customer.email = email;
                                       weakSelf.customer.password = password;
                                      
                                     }
                                     NSDictionary *jsonResponse = responseObject;
                                     jsonResponse = jsonResponse[@"customer"];
                                     weakSelf.authToken = jsonResponse[@"auth_token"];
                                     weakSelf.customerId = [jsonResponse[@"id"] stringValue];
                                     weakSelf.sessionId = jsonResponse[@"session"];
                                     weakSelf.token = jsonResponse[@"token"];
                                     weakSelf.customer.customerId = weakSelf.customerId;
//                                     [self.cingoWebServices getCustomerInformation:^(NSURLResponse *response, id responseObject, NSError *error) {
//                                       
//                                     }];
                                   }
                                 }
                                 [weakSelf saveSignedUpCustomer:weakSelf.customer];
                                 webServicesResponse(weakSelf.customer,error);
                                 
                               }];
}
//- (void)signUpNewCustomer:(NSString*)email password:(NSString*)password firstName:(NSString*)firstName lastName:(NSString*)lastName mobile:(NSString*)mobileNumber{
//  CingoCustomer *cingoCustomer = [[CingoCustomer alloc]init];
//  [cingoCustomer setEmail:email];
//  [cingoCustomer setPassword:password];
//  [cingoCustomer setFirstName:firstName];
//  [cingoCustomer setLastName:lastName];
//  [cingoCustomer setMobileNumber:mobileNumber];
//  [self.cingoWebServices signUpNewCustomer:[cingoCustomer jsonObject] callback:^(NSURLResponse *response, id responseObject, NSError *error) {
//    
//    
//    
//  }];
//  
//}

- (void)signUpNewCustomer:(CingoCustomer*)customer callback:(CustomerWebServicesResponse)webServicesResponse{
  
  [self.cingoWebServices signUpNewCustomer:[customer jsonObject] callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    if (!error){
    NSDictionary *jsonResponse = (NSDictionary*)responseObject;
    [customer setCustomerId:[[jsonResponse objectForKey:@"customer_id"]stringValue]];
    weakSelf.customer = customer;
      [weakSelf saveSignedUpCustomer:customer];
      webServicesResponse(customer,error);
    }
    else{
      if(responseObject){
        NSDictionary *jsonResponse = (NSDictionary*)responseObject;
        NSArray *jsonError = [jsonResponse objectForKey:@"errors"];
        
        if([jsonError isKindOfClass:[NSArray class]] && [jsonError count]>0){
          NSString *errorMessage = jsonError[0];
          NSError *cingoError =[NSError errorWithDomain:error.domain code:error.code userInfo:@{@"error":errorMessage}];
          webServicesResponse(customer,cingoError);
        
        }
        else if([jsonError isKindOfClass:[NSDictionary class]]){
       webServicesResponse(customer,error);
        }
      }
      else{
        webServicesResponse(customer,error);
      }
    }
   
    
  }];
}
- (void)changeCustomerSettings:(CingoCustomer*)customer callback:(CustomerWebServicesResponse)webServicesResponse {
  [self.cingoWebServices changeCustomerInformation:[customer jsonObject] callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    if (!error){
      NSDictionary *jsonResponse = (NSDictionary*)responseObject;
      [customer setCustomerId:[jsonResponse objectForKey:@"customer_id"]];
      weakSelf.customer = customer;
      [weakSelf saveSignedUpCustomer:customer];
      webServicesResponse(customer,error);
    }
    else{
      if(responseObject){
        NSDictionary *jsonResponse = (NSDictionary*)responseObject;
        NSArray *jsonError = [jsonResponse objectForKey:@"errors"];
        
        if([jsonError isKindOfClass:[NSArray class]] && [jsonError count]>0){
          NSString *errorMessage = jsonError[0];
          NSError *cingoError =[NSError errorWithDomain:error.domain code:error.code userInfo:@{@"error":errorMessage}];
          webServicesResponse(customer,cingoError);
          
        }
        else if([jsonError isKindOfClass:[NSDictionary class]]){
          webServicesResponse(customer,error);
        }
      }
      else{
        webServicesResponse(customer,error);
      }
    }

  }];
}

- (void)changeCustomerSettings:(NSString*)email password:(NSString*)password firstName:(NSString*)firstName lastName:(NSString*)lastName mobile:(NSString*)mobileNumber{
  CingoCustomer *cingoCustomer = [[CingoCustomer alloc]init];
  [cingoCustomer setEmail:email];
  [cingoCustomer setPassword:password];
  [cingoCustomer setFirstName:firstName];
  [cingoCustomer setLastName:lastName];
  [cingoCustomer setMobileNumber:mobileNumber];
  [self.cingoWebServices changeCustomerInformation:[cingoCustomer jsonObject] callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    
  }];
}


- (void)fetchAllUserRequests:(WebServicesResponse)webServicesResponse{
  [self.cingoWebServices fetchAllUserRequests:webServicesResponse];
}
- (void)fetchRequestFor:(NSString*)requestId callback:(WebServicesResponse)webServicesResponse{
  [self.cingoWebServices fetchRequestFor:requestId callback:webServicesResponse];
}
//- (void)createNewRequest:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse{
//  [self.cingoWebServices createNewRequest:jsonToPost callback:webServicesResponse];
//  
//}
- (void)createNewRequest:(CingoRequest*)request callback:(RequestWebServicesResponse)webServicesResponse{
  [request setCustomerId:self.customer.customerId];
  [request setVendorId:@"2"];
   [self.cingoWebServices createNewRequest:[request jsonObject] callback:^(NSURLResponse *response, id responseObject, NSError *error) {
     if (!error){
       NSDictionary *jsonResponse = (NSDictionary*)responseObject;
       request.requestId = [jsonResponse[@"user_request_id"] stringValue];
       webServicesResponse(request,error);
       
     }
     else{
       webServicesResponse(nil,error);
     }
     
     
   }];
}
- (void)changeRequestInformation:(NSString*)requestId  json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse{
  [self.cingoWebServices changeRequestInformation:requestId json:jsonToPost callback:webServicesResponse];
  
}
- (void)postCustomerRatingFor:(NSString*)requestId json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse{
  [self.cingoWebServices postCustomerRatingFor:requestId json:jsonToPost callback:webServicesResponse];
  
}
- (void)addRequestNotesFor:(NSString*)requestId json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse{
  [self.cingoWebServices addRequestNotesFor:requestId json:jsonToPost callback:webServicesResponse];
}

- (void)saveSignedUpCustomer:(CingoCustomer*)customer{
  [CingoUtils saveObject:[customer jsonObject] forKey:@"currentUser"];
}

- (void)loadSavedCustomer{
  NSDictionary *savedCustomer = [CingoUtils objectForKey:@"currentUser"];
  if (savedCustomer){
    self.customer = [CingoCustomer jsonToObject:savedCustomer];
  }
}

- (NSString*)expectedCallSignalString{
  return [NSString stringWithFormat:@"userrequest_%@",self.customer.customerId];
}
- (NSString*)expectedAcceptCallSignalString{
  return [NSString stringWithFormat:@"accept_%@",self.currentReqeustId];
}
@end
