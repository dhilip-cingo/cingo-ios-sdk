//
//  CingoUtils.h
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface CingoUtils : NSObject
+ (void)showAlertView:(NSString*)title message:(NSString*)message button1:(NSString*)button1 button1Action:(void (^ __nullable)(UIAlertAction *action))button1Action otherButton:(NSString*)cancelButton;
+ (void)saveObject:(NSDictionary*)object forKey:(NSString*)key;
+ (NSDictionary*)objectForKey:(NSString*)key;
@end
