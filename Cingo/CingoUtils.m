//
//  CingoUtils.m
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "CingoUtils.h"

@implementation CingoUtils
+ (void)showAlertView:(NSString*)title message:(NSString*)message button1:(NSString*)button1 button1Action:(void (^ __nullable)(UIAlertAction *action))button1Action otherButton:(NSString*)cancelButton{
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert title" message:@"Alert message" preferredStyle:UIAlertControllerStyleAlert];
  
  
  UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelButton style:UIAlertActionStyleDefault handler:nil];
  [alertController addAction:cancel];
  if(button1){
    UIAlertAction* customAction = [UIAlertAction actionWithTitle:button1 style:UIAlertActionStyleDefault handler:button1Action ];
    [alertController addAction:customAction];
  
  }

  UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
  [topVC presentViewController:alertController animated:YES completion:nil];
}
+ (void)saveObject:(NSDictionary*)object forKey:(NSString*)key{
  [[NSUserDefaults standardUserDefaults]setObject:object forKey:key];
  [[NSUserDefaults standardUserDefaults]synchronize];
}

+ (NSDictionary*)objectForKey:(NSString*)key{
  return [[NSUserDefaults standardUserDefaults]objectForKey:key];
}
@end
