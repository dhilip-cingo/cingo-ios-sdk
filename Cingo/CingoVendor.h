//
//  CingoVendor.h
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CingoVendor : NSObject
@property (nonatomic,strong)NSString *vendorId,*vendorAccountNumber;
- (NSDictionary*)jsonObject;
@end
