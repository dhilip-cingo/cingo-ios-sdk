//
//  CingoVendor.m
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "CingoVendor.h"

@implementation CingoVendor

-(NSDictionary*)jsonObject{
 return @{
  @"vendor_id": self.vendorId,
  @"account_number": self.vendorAccountNumber
  };
}
@end
