//
//  WebServices.h
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CingoSession.h"
#import "CingoRESTServices.h"
@interface CingoWebServices : NSObject
@property (nonatomic,weak)CingoSession *cingoSession;

- (void)initializeWebServicesSession;
- (void)signInCustomer:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse;
- (void)signUpNewCustomer:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse;
- (void)changeCustomerInformation:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse;
- (void)getCustomerInformation:(WebServicesResponse)webServicesResponse;
- (void)getVendorInformation:(WebServicesResponse)webServicesResponse;
- (void)fetchAllUserRequests:(WebServicesResponse)webServicesResponse;
- (void)fetchRequestFor:(NSString*)requestId callback:(WebServicesResponse)webServicesResponse;
- (void)createNewRequest:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse;
- (void)changeRequestInformation:(NSString*)requestId  json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse;
- (void)postCustomerRatingFor:(NSString*)requestId json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse;
- (void)addRequestNotesFor:(NSString*)requestId json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse;

@end
