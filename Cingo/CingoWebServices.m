//
//  WebServices.m
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "CingoWebServices.h"
#import "Cingo.h"
#import "CingoRESTServices.h"
#import <AFNetworking/AFNetworking.h>

const NSString *GETHttpMethod = @"GET";
const NSString *POSTTHttpMethod = @"POST";
const NSString *PUTHttpMethod = @"PUT";
const NSString *mainURL = @"https://staging.cingo.co";
const NSString *signInURL = @"/api/v1/customers/sign_in";
const NSString *signUpURL = @"/api/v1/customers";
const NSString *customerInfoEditURL = @"/api/v1/customers/";
@implementation CingoWebServices

- (void)initializeWebServicesSession{
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
}

- (NSMutableURLRequest*)constructRequestFor:(NSString*)requestURL forHTTPMethod:(NSString*)method parameters:(NSDictionary*)parameters{
  NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]requestWithMethod:method URLString:[NSString stringWithFormat:@"%@%@",mainURL,requestURL] parameters:parameters error:nil];
  [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  return request;
}

- (void)addAuthorizationHeader:(NSMutableURLRequest*)request{
    [request setValue:self.cingoSession.authToken forHTTPHeaderField:@"Authorization"];
}

- (NSData*)converToJSONData:(NSObject*)object{
  NSError *jsonError = nil;
  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&jsonError];
  NSParameterAssert(jsonData);
  return jsonData;
}
- (void)signInCustomer:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse{
  NSAssert(jsonToPost != nil, @"Json to post was nil");
  NSMutableURLRequest *request = [self constructRequestFor:signInURL forHTTPMethod:POSTTHttpMethod parameters:nil];
  NSData *jsonData = [self converToJSONData:jsonToPost];
  [request setHTTPBody:jsonData];
  CingoRESTServices *service = [[CingoRESTServices alloc]init];
  [service dataRequest:request callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    webServicesResponse(response,responseObject,error);
  }];
}

- (void)signUpNewCustomer:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse{
  NSAssert(jsonToPost != nil, @"Json to post was nil");
  DLog(@"Signup JSON %@",jsonToPost);
  NSMutableURLRequest *request = [self constructRequestFor:signUpURL forHTTPMethod:POSTTHttpMethod parameters:nil];
  NSData *jsonData = [self converToJSONData:jsonToPost];
  [request setHTTPBody:jsonData];
  CingoRESTServices *service = [[CingoRESTServices alloc]init];
  [service dataRequest:request callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    webServicesResponse(response,responseObject,error);
  }];
}

- (void)changeCustomerInformation:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse{
  NSAssert(jsonToPost != nil, @"Json to post was nil");
  NSString *customerInfoURLString = [customerInfoEditURL stringByAppendingFormat:@"%@",_cingoSession.customer.customerId];
  NSMutableURLRequest *request = [self constructRequestFor:customerInfoURLString forHTTPMethod:PUTHttpMethod parameters:nil];
  [self addAuthorizationHeader:request];
  NSData *jsonData = [self converToJSONData:jsonToPost];
  [request setHTTPBody:jsonData];
  CingoRESTServices *service = [[CingoRESTServices alloc]init];
  [service dataRequest:request callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    webServicesResponse(response,responseObject,error);
  }];
}

- (void)getCustomerInformation:(WebServicesResponse)webServicesResponse{
  NSString *allRequestsURLString = [NSString stringWithFormat:@"/api/v1/customers/%@?vendor_id=%@", _cingoSession.customer.customerId,_cingoSession.vendor.vendorId];
  NSMutableURLRequest *request = [self constructRequestFor:allRequestsURLString forHTTPMethod:GETHttpMethod parameters:nil];
  CingoRESTServices *service = [[CingoRESTServices alloc]init];
  [service dataRequest:request callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    webServicesResponse(response,responseObject,error);
  }];
}

- (void)getVendorInformation:(WebServicesResponse)webServicesResponse{
  NSString *allRequestsURLString = [NSString stringWithFormat:@"/api/v1/vendors/%@", _cingoSession.vendor.vendorId];
  NSMutableURLRequest *request = [self constructRequestFor:allRequestsURLString forHTTPMethod:GETHttpMethod parameters:nil];
  CingoRESTServices *service = [[CingoRESTServices alloc]init];
  [service dataRequest:request callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    webServicesResponse(response,responseObject,error);
  }];
}
- (void)fetchAllUserRequests:(WebServicesResponse)webServicesResponse{
  NSString *allRequestsURLString = [NSString stringWithFormat:@"/api/v1/customers/%@/user_requests", _cingoSession.customer.customerId];
  NSMutableURLRequest *request = [self constructRequestFor:allRequestsURLString forHTTPMethod:GETHttpMethod parameters:nil];
  CingoRESTServices *service = [[CingoRESTServices alloc]init];
  [service dataRequest:request callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    webServicesResponse(response,responseObject,error);
  }];
}

- (void)fetchRequestFor:(NSString*)requestId callback:(WebServicesResponse)webServicesResponse{
   NSAssert(requestId != nil, @"Json to post was nil");
  NSString *requestURLString = [NSString stringWithFormat:@"/api/v1/customers/%@/user_requests/%@", _cingoSession.customer.customerId,requestId];
  NSMutableURLRequest *request = [self constructRequestFor:requestURLString forHTTPMethod:GETHttpMethod parameters:nil];
  CingoRESTServices *service = [[CingoRESTServices alloc]init];
  [service dataRequest:request callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    webServicesResponse(response,responseObject,error);
  }];
}

- (void)createNewRequest:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse{
  NSAssert(jsonToPost != nil, @"Json to post was nil");
  NSString *allRequestsURLString = [NSString stringWithFormat:@"/api/v1/customers/%@/user_requests", _cingoSession.customer.customerId];
  NSMutableURLRequest *request = [self constructRequestFor:allRequestsURLString forHTTPMethod:POSTTHttpMethod parameters:nil];
  [self addAuthorizationHeader:request];
  NSData *jsonData = [self converToJSONData:jsonToPost];
  [request setHTTPBody:jsonData];
  CingoRESTServices *service = [[CingoRESTServices alloc]init];
  [service dataRequest:request callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    
    webServicesResponse(response,responseObject,error);
  }];
}

- (void)changeRequestInformation:(NSString*)requestId  json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse{
  NSAssert(jsonToPost != nil, @"Json to post was nil");
 NSString *allRequestsURLString = [NSString stringWithFormat:@"/api/v1/customers/%@/user_requests/%@", _cingoSession.customer.customerId,requestId];
  NSMutableURLRequest *request = [self constructRequestFor:allRequestsURLString forHTTPMethod:PUTHttpMethod parameters:nil];
  NSData *jsonData = [self converToJSONData:jsonToPost];
  [request setHTTPBody:jsonData];
  CingoRESTServices *service = [[CingoRESTServices alloc]init];
  [service dataRequest:request callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    webServicesResponse(response,responseObject,error);
  }];
}

- (void)postCustomerRatingFor:(NSString*)requestId json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse{
  NSAssert(jsonToPost != nil, @"Json to post was nil");
  NSString *allRequestsURLString = [NSString stringWithFormat:@"/api/v1/customers/%@/user_requests/%@/user_requests_rating", _cingoSession.customer.customerId,requestId];
  NSMutableURLRequest *request = [self constructRequestFor:allRequestsURLString forHTTPMethod:POSTTHttpMethod parameters:nil];
  NSData *jsonData = [self converToJSONData:jsonToPost];
  [request setHTTPBody:jsonData];
  CingoRESTServices *service = [[CingoRESTServices alloc]init];
  [service dataRequest:request callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    webServicesResponse(response,responseObject,error);
  }];

  
}

- (void)addRequestNotesFor:(NSString*)requestId json:(NSDictionary*)jsonToPost callback:(WebServicesResponse)webServicesResponse{
  NSAssert(jsonToPost != nil, @"Json to post was nil");
  NSString *allRequestsURLString = [NSString stringWithFormat:@"/api/v1/customers/%@/user_requests/%@/user_requests_notes", _cingoSession.customer.customerId,requestId];
  NSMutableURLRequest *request = [self constructRequestFor:allRequestsURLString forHTTPMethod:POSTTHttpMethod parameters:nil];
  [self addAuthorizationHeader:request];
  NSData *jsonData = [self converToJSONData:jsonToPost];
  [request setHTTPBody:jsonData];
  CingoRESTServices *service = [[CingoRESTServices alloc]init];
  [service dataRequest:request callback:^(NSURLResponse *response, id responseObject, NSError *error) {
    webServicesResponse(response,responseObject,error);
  }];
  
  
}


@end
