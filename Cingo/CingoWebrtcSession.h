//
//  CingoWebrtcSession.h
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CingoSession.h"
#import <OpenTok/OpenTok.h>
@interface CingoWebrtcSession : NSObject
@property (nonatomic,weak)CingoSession *cingoSession;
@property (nonatomic,strong)OTSession* session;
@property (nonatomic,strong)OTPublisher* publisher;
@property (nonatomic,strong)OTSubscriber* subscriber;
- (void)initializeSession;
- (void)dial;
- (void)endCall;
- (void)sendSignalMessage:(NSString*)type message:(NSString*)message;
@end
