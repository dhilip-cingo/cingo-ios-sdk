//
//  CingoWebrtcSession.m
//  Cingo
//
//  Created by Dhilip on 5/8/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "CingoWebrtcSession.h"
#import "Cingo.h"

@interface CingoWebrtcSession ()<OTSessionDelegate, OTSubscriberKitDelegate, OTPublisherDelegate>{
  BOOL subscribeToSelf;
  OTStream *incomingStream;
}
@end


@implementation CingoWebrtcSession

- (void)initializeSession{
  _session = [[OTSession alloc] initWithApiKey:kApiKey
                                     sessionId:[[CingoSession sharedCingoSession]sessionId]
                                      delegate:self];
  subscribeToSelf = false;
  [self doConnect];
}

- (void)dial{
  [self doSubscribe:incomingStream];
  [self doPublish];
}

- (void)endCall{
  [self cleanupPublisher];
  [self cleanupSubscriber];
  [_session disconnect:nil];
  [self doConnect];
}

- (void)sendSignalMessage:(NSString*)type message:(NSString*)message{
  DLog(@"Signal Type %@ Message %@",type,message);
  [_session signalWithType:type string:message connection:nil error:nil];
}
- (void)doConnect
{
  OTError *error = nil;
  
  [_session connectWithToken:[[CingoSession sharedCingoSession]token] error:&error];
  if (error)
    {
    DLog(@"Connection Error %@",[error localizedDescription]);
    }
}

- (void)doPublish
{
  _publisher =
  [[OTPublisher alloc] initWithDelegate:self
                                   name:self.cingoSession.customer.fullName];
  
//  _publisher =
//  [[OTPublisherKit alloc] initWithDelegate:self name:self.cingoSession.customer.fullName audioTrack:true videoTrack:false];
  
  OTError *error = nil;
  [_session publish:_publisher error:&error];
  if (error)
    {
     DLog(@"Connection Error %@",[error localizedDescription]);
    }
}
- (void)cleanupPublisher {
  
  [_publisher.view removeFromSuperview];
  _publisher = nil;
  // this is a good place to notify the end-user that publishing has stopped.
}

/**
 * Instantiates a subscriber for the given stream and asynchronously begins the
 * process to begin receiving A/V content for this stream. Unlike doPublish,
 * this method does not add the subscriber to the view hierarchy. Instead, we
 * add the subscriber only after it has connected and begins receiving data.
 */
- (void)doSubscribe:(OTStream*)stream
{
  _subscriber = [[OTSubscriber alloc] initWithStream:stream delegate:self];
  
  OTError *error = nil;
  [_session subscribe:_subscriber error:&error];
  if (error)
    {
    DLog(@"Connection Error %@",[error localizedDescription]);
    }
}

/**
 * Cleans the subscriber from the view hierarchy, if any.
 * NB: You do *not* have to call unsubscribe in your controller in response to
 * a streamDestroyed event. Any subscribers (or the publisher) for a stream will
 * be automatically removed from the session during cleanup of the stream.
 */
- (void)cleanupSubscriber
{
  [_subscriber.view removeFromSuperview];
  _subscriber = nil;
}


# pragma mark - OTSession delegate callbacks

- (void)sessionDidConnect:(OTSession*)session
{
  DLog(@"sessionDidConnect (%@)", session.sessionId);
  
  // Step 2: We have successfully connected, now instantiate a publisher and
  // begin pushing A/V streams into OpenTok.
  //[self doPublish];
}

- (void)sessionDidDisconnect:(OTSession*)session
{
  NSString* alertMessage =
  [NSString stringWithFormat:@"Session disconnected: (%@)",
   session.sessionId];
  DLog(@"sessionDidDisconnect (%@)", alertMessage);
}


- (void)session:(OTSession*)mySession
  streamCreated:(OTStream *)stream
{
  DLog(@"session streamCreated (%@)", stream.streamId);
  
  // Step 3a: (if NO == subscribeToSelf): Begin subscribing to a stream we
  // have seen on the OpenTok session.
  if (nil == _subscriber && !subscribeToSelf)
    {
    incomingStream = stream;
    [self doSubscribe:stream];
    }
}

- (void)session:(OTSession*)session
streamDestroyed:(OTStream *)stream
{
  DLog(@"session streamDestroyed (%@)", stream.streamId);
  
  if ([_subscriber.stream.streamId isEqualToString:stream.streamId])
    {
    [self cleanupSubscriber];
    }
}

- (void)  session:(OTSession *)session
connectionCreated:(OTConnection *)connection
{
  DLog(@"session connectionCreated (%@)", connection.connectionId);
}

- (void)    session:(OTSession *)session
connectionDestroyed:(OTConnection *)connection
{
  DLog(@"session connectionDestroyed (%@)", connection.connectionId);
  if ([_subscriber.stream.connection.connectionId
       isEqualToString:connection.connectionId])
    {
    [self cleanupSubscriber];
    }
}

- (void) session:(OTSession*)session
didFailWithError:(OTError*)error
{
  DLog(@"didFailWithError: (%@)", error);
}

- (void)session:(OTSession *)session receivedSignalType:(NSString *)type fromConnection:(OTConnection *)connection withString:(NSString *)string{
  DLog(@"Type %@ - %@",type,string);
  if ([type isEqualToString:[[CingoSession sharedCingoSession]expectedCallSignalString]]){
    if ([[CingoSession sharedCingoSession]delegate] && [[[CingoSession sharedCingoSession]delegate]respondsToSelector:@selector(sessionCallInitiated)]) {
      [self sendSignalMessage:[[CingoSession sharedCingoSession]expectedAcceptCallSignalString] message:@"Dhilip Again"];
      [[[CingoSession sharedCingoSession]delegate]sessionCallInitiated];
    }
  }
}
# pragma mark - OTSubscriber delegate callbacks

- (void)subscriberDidConnectToStream:(OTSubscriberKit*)subscriber
{
  DLog(@"subscriberDidConnectToStream (%@)",
        subscriber.stream.connection.connectionId);
  assert(_subscriber == subscriber);
//  [_subscriber.view setFrame:CGRectMake(0, widgetHeight, widgetWidth,
//                                        widgetHeight)];
//  [self.view addSubview:_subscriber.view];
  if ([[CingoSession sharedCingoSession]delegate] && [[[CingoSession sharedCingoSession]delegate]respondsToSelector:@selector(streamAvailableForRendering:)]) {
    [[[CingoSession sharedCingoSession]delegate]streamAvailableForRendering:_subscriber.view];
  }
  
}

- (void)subscriber:(OTSubscriberKit*)subscriber
  didFailWithError:(OTError*)error
{
  DLog(@"subscriber %@ didFailWithError %@",
        subscriber.stream.streamId,
        error);
}

# pragma mark - OTPublisher delegate callbacks

- (void)publisher:(OTPublisherKit *)publisher
    streamCreated:(OTStream *)stream
{
  // Step 3b: (if YES == subscribeToSelf): Our own publisher is now visible to
  // all participants in the OpenTok session. We will attempt to subscribe to
  // our own stream. Expect to see a slight delay in the subscriber video and
  // an echo of the audio coming from the device microphone.
  DLog(@"Publisher stream created");
  if (nil == _subscriber && subscribeToSelf)
    {
    [self doSubscribe:stream];
    }
}

- (void)publisher:(OTPublisherKit*)publisher
  streamDestroyed:(OTStream *)stream
{
  DLog(@"Publisher stream destroyed");
  if ([_subscriber.stream.streamId isEqualToString:stream.streamId])
    {
    [self cleanupSubscriber];
    }
  
  [self cleanupPublisher];
}

- (void)publisher:(OTPublisherKit*)publisher
 didFailWithError:(OTError*) error
{
  DLog(@"publisher didFailWithError %@", error);
  [self cleanupPublisher];
}

- (void)showAlert:(NSString *)string
{
  // show alertview on main UI
  dispatch_async(dispatch_get_main_queue(), ^{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OTError"
                                                    message:string
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil] ;
    [alert show];
  });
}

@end
