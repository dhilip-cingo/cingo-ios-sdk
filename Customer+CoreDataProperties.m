//
//  Customer+CoreDataProperties.m
//  
//
//  Created by Dhilip on 5/25/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Customer+CoreDataProperties.h"

@implementation Customer (CoreDataProperties)

@dynamic fullName;
@dynamic email;
@dynamic phoneNumber;
@dynamic password;
@dynamic customerId;
@dynamic street;
@dynamic zipcode;
@dynamic state;
@dynamic city;

@end
