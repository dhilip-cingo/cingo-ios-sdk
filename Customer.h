//
//  Customer.h
//  
//
//  Created by Dhilip on 5/25/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Customer : NSManagedObject
+(Customer*)currentCustomer;
// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Customer+CoreDataProperties.h"
