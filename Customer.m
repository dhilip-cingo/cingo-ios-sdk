//
//  Customer.m
//  
//
//  Created by Dhilip on 5/25/16.
//
//

#import "Customer.h"
#import "ObjectiveRecord.h"
@implementation Customer
+(Customer*)currentCustomer{
  NSArray *customers = [Customer all];
  Customer *currentCustomer = [customers lastObject];
  return currentCustomer;
}
// Insert code here to add functionality to your managed object subclass

@end
