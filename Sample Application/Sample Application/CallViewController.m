//
//  CallViewController.m
//  Sample Application
//
//  Created by Dhilip on 5/26/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "CallViewController.h"
#import <Cingo/Cingo.h>
@interface CallViewController ()<CingoSessionDelegate>

@end

@implementation CallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  [[CingoSession sharedCingoSession]setDelegate:self];
  [[CingoSession sharedCingoSession]dial];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)streamAvailableForRendering:(UIView *)subscriber{
  [subscriber setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
  [self.view addSubview:subscriber];

}
@end
