//
//  DataController.h
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cingo/Cingo.h>
@interface DataController : NSObject
@property (nonatomic,strong) CingoSession *cingoSession;
@end
