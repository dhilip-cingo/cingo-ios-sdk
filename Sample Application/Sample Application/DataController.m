//
//  DataController.m
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "DataController.h"

static DataController *dataController = nil;
@implementation DataController
+(DataController*)sharedDataController{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    dataController = [[DataController alloc]init];
  });
  return dataController;
}


@end
