//
//  NewRequestViewController.h
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewRequestViewController : UITableViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>
@property(nonatomic,strong)IBOutlet UITextView *requestTextView;
@property(nonatomic,strong)IBOutlet UITextField *callbackTimeTextField,*departmentTextField;
@property(nonatomic,weak)IBOutlet UIImageView *thumbImageView;
- (IBAction)attachmentTapped:(id)sender;
- (IBAction)createTapped:(id)sender;

@end
