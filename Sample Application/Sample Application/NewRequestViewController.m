//
//  NewRequestViewController.m
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "NewRequestViewController.h"
#import "Request.h"
#import "ObjectiveRecord.h"
#import "Utils.h"
#import <Cingo/Cingo.h>
@interface NewRequestViewController ()<UIImagePickerControllerDelegate>
@property(nonatomic,strong)UIPickerView *picker;
@property(nonatomic,strong)UIDatePicker *timePicker;
@property(nonatomic,strong)NSArray *departmentDataSource;
@property(nonatomic,strong) NSDateFormatter *timeFormatter;
@end

@implementation NewRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  [self createTimePicker];
  [self createPicker];
  _picker.dataSource = self;
  _picker.delegate=self;
  self.timeFormatter = [[NSDateFormatter alloc]init];
  _timeFormatter.timeStyle = NSDateFormatterShortStyle;
  _departmentDataSource = @[@"General",@"Other"];
  _callbackTimeTextField.inputView = _timePicker;
  _departmentTextField.inputView = _picker;
  self.title = @"New Request";
  

    _callbackTimeTextField.text = [_timeFormatter stringFromDate:[[NSDate date]dateByAddingTimeInterval:1800]];

    _departmentTextField.text = _departmentDataSource[0];
  
}

- (void)viewDidAppear:(BOOL)animated{
  [super viewDidAppear:animated];
  self.requestTextView.layer.borderWidth = 1.0f;
  self.requestTextView.layer.borderColor = [[UIColor grayColor] CGColor];
}

- (void)createTimePicker{
  _timePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 155)];
  _timePicker.datePickerMode = UIDatePickerModeTime;
  NSDate *currentTime = [NSDate new];
  currentTime  = [currentTime dateByAddingTimeInterval:1800];
  _timePicker.minimumDate = currentTime;
  [_timePicker addTarget:self action:@selector(callBackTimeChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)createPicker{
  _picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 155)];
 
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)callBackTimeChanged:(UIDatePicker*)datePicker{

  _callbackTimeTextField.text = [self.timeFormatter stringFromDate:datePicker.date];
}

- (void)departmentValueChanged:(UIPickerView*)picker{
  _departmentTextField.text = _departmentDataSource[[picker selectedRowInComponent:0]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)textFieldDidBeginEditing:(UITextField *)textField{
  
}
- (IBAction)createTapped:(id)sender{
  CingoRequest *request = [[CingoRequest alloc]init];
  [request setRequestTime:_callbackTimeTextField.text];
  [request setDepartmentId:_departmentTextField.text];
  [request setRequestDescription:_requestTextView.text];
  [[CingoSession sharedCingoSession]createNewRequest:request callback:^(CingoRequest *request, NSError *error) {
    if (!error){
      Request *req = [Request create];
      [req setVendorId:request.vendorId];
      [req setRequestDescription:request.requestDescription];
      [req setRequestTime:request.requestTime];
      [req setCustomerId:request.customerId];
      [req setDepartmentId:request.departmentId];
      [req setAttachmentDetails:request.attachmentDetails];
      [req setRequestId:request.requestId];
      [req save];
      if (self.thumbImageView.image) {
      [Utils writeImageToFile:request.requestId image:self.thumbImageView.image];
      }
      
      dispatch_async(dispatch_get_main_queue(), ^{
        [self.view endEditing:true];
        [Utils showSuccessAlert:@"Request Created"];
        [self.tabBarController setSelectedIndex:0];
      });
      
    }
  }];
}
- (IBAction)attachmentTapped:(id)sender{
  UIAlertController* alert = [UIAlertController
                              alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                              message:nil
                              preferredStyle:UIAlertControllerStyleActionSheet];
  
  UIAlertAction* button0 = [UIAlertAction
                            actionWithTitle:@"Cancel"
                            style:UIAlertActionStyleCancel
                            handler:^(UIAlertAction * action)
                            {
                              //  UIAlertController will automatically dismiss the view
                            }];
  
  UIAlertAction* button1 = [UIAlertAction
                            actionWithTitle:@"Take photo"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                              //  The user tapped on "Take a photo"
                              UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
                              imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                              imagePickerController.delegate = self;
                              [self presentViewController:imagePickerController animated:YES completion:^{}];
                            }];
  
  UIAlertAction* button2 = [UIAlertAction
                            actionWithTitle:@"Choose Existing"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                              //  The user tapped on "Choose existing"
                              UIImagePickerController *imagePickerController= [[UIImagePickerController alloc] init];
                              imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                              imagePickerController.delegate = self;
                              [self presentViewController:imagePickerController animated:YES completion:^{}];
                            }];
  [alert addAction:button0];
  [alert addAction:button1];
  [alert addAction:button2];
  [self presentViewController:alert animated:YES completion:^{
    
  }];
}
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
  return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
  return [_departmentDataSource count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
  return _departmentDataSource[row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
  [self departmentValueChanged:pickerView];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
     UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
  self.thumbImageView.image = image;
  
   [picker dismissViewControllerAnimated:YES completion:nil];
  
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
  [picker dismissViewControllerAnimated:YES completion:nil];
}
@end
