//
//  NotesTableViewCell.h
//  Sample Application
//
//  Created by Dhilip on 5/26/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotesTableViewCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UILabel *authorLabel, *notesLabel;

@end
