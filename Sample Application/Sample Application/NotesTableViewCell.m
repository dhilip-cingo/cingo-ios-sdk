//
//  NotesTableViewCell.m
//  Sample Application
//
//  Created by Dhilip on 5/26/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "NotesTableViewCell.h"

@implementation NotesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
