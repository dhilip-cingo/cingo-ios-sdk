//
//  Request+CoreDataProperties.h
//  
//
//  Created by Dhilip Raveendran on 5/27/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Request.h"

NS_ASSUME_NONNULL_BEGIN

@interface Request (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *attachmentDetails;
@property (nullable, nonatomic, retain) NSString *customerId;
@property (nullable, nonatomic, retain) NSString *departmentId;
@property (nullable, nonatomic, retain) NSString *requestDescription;
@property (nullable, nonatomic, retain) NSString *requestId;
@property (nullable, nonatomic, retain) NSString *requestTime;
@property (nullable, nonatomic, retain) NSString *vendorId;
@property (nullable, nonatomic, retain) NSString *notes;

@end

NS_ASSUME_NONNULL_END
