//
//  Request+CoreDataProperties.m
//  
//
//  Created by Dhilip Raveendran on 5/27/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Request+CoreDataProperties.h"

@implementation Request (CoreDataProperties)

@dynamic attachmentDetails;
@dynamic customerId;
@dynamic departmentId;
@dynamic requestDescription;
@dynamic requestId;
@dynamic requestTime;
@dynamic vendorId;
@dynamic notes;

@end
