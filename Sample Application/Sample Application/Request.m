//
//  Request.m
//  
//
//  Created by Dhilip on 5/26/16.
//
//

#import "Request.h"

@implementation Request

// Insert code here to add functionality to your managed object subclass
- (NSDictionary*)notesJsonObject{
  return @{
           @"note" :
             @{
               @"description": self.notes,
               @"added_by_id": self.customerId,
               @"added_by_type": @"Customer"
               }
           };
}
@end
