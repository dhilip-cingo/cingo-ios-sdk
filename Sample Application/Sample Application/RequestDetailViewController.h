//
//  RequestDetailViewController.h
//  Sample Application
//
//  Created by Dhilip on 5/26/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request.h"
@interface RequestDetailViewController : UIViewController
@property(nonatomic,weak)IBOutlet UITableView *notesTableView;
@property(nonatomic,weak)IBOutlet UILabel *statusTableView,*timeLabelView;
@property(nonatomic,weak)IBOutlet UITextView *requestDescriptionTextView,*notesTextView;
@property(nonatomic,weak)IBOutlet UIImageView *attachmentView;
@property(nonatomic,weak)IBOutlet UIBarButtonItem *notesBarButton, *callBarButton;
@property(nonatomic,strong)Request *selectedRequest;
- (IBAction)addNoteTapped;
- (IBAction)chatTapped;
- (IBAction)callTapped:(UIBarButtonItem*)sender;
@end
