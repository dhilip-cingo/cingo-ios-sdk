//
//  RequestDetailViewController.m
//  Sample Application
//
//  Created by Dhilip on 5/26/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "RequestDetailViewController.h"
#import "NotesTableViewCell.h"
#import <Cingo/Cingo.h>
#import "Sample_Application-Swift.h"
#import "ObjectiveRecord.h"
#import "Utils.h"

//@interface RequestDetailViewController ()<LGChatControllerDelegate>
//
//@end
@interface RequestDetailViewController ()<CingoSessionDelegate,LGChatControllerDelegate>
@property (nonatomic,strong) UIView *subscriberView;
@end
@implementation RequestDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  self.requestDescriptionTextView.text = self.selectedRequest.requestDescription;
  self.timeLabelView.text = self.selectedRequest.requestTime;
  
  [[CingoSession sharedCingoSession]setCurrentReqeustId:self.selectedRequest.requestId];
  [[CingoSession sharedCingoSession]setDelegate:self];
  if (self.selectedRequest.notes){
    self.notesTextView.text = self.selectedRequest.notes;
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.notesBarButton setEnabled:NO];
    });
  }
  UIImage *image = [Utils imageForFile:self.selectedRequest.requestId];

  self.attachmentView.image = image;
  
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated{
  [super viewDidAppear:animated];
  self.requestDescriptionTextView.layer.borderWidth = 1.0f;
  self.requestDescriptionTextView.layer.borderColor = [[UIColor grayColor] CGColor];
  self.notesTextView.layer.borderWidth = 1.0f;
  self.notesTextView.layer.borderColor = [[UIColor grayColor] CGColor];
  
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)addNoteTapped{
  UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Note"
                                                                            message: nil
                                                                     preferredStyle:UIAlertControllerStyleAlert];
  [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    textField.placeholder = @"Add Note";
    textField.textColor = [UIColor blueColor];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.borderStyle = UITextBorderStyleRoundedRect;
  }];
  
  [alertController addAction:[UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    NSArray * textfields = alertController.textFields;
    UITextField * namefield = textfields[0];
    [self addNotes:namefield.text];
    
  }]];
  
  [alertController addAction:[UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

    
  }]];
  
  [self presentViewController:alertController animated:YES completion:nil];
}

- (void)addNotes:(NSString*)note{
  if(note!=nil){
    self.selectedRequest.notes = note;
    [[CingoSession sharedCingoSession]addRequestNotesFor:self.selectedRequest.requestId json:[self.selectedRequest notesJsonObject] callback:^(NSURLResponse *response, id responseObject, NSError *error) {
      if(!error){
        self.selectedRequest.notes = note;
        [self.selectedRequest save];
        [Utils showSuccessAlert:@"Note Added"];
        dispatch_async(dispatch_get_main_queue(), ^{
        [self.notesBarButton setEnabled:NO];
        });
        
      }
    }];
  }
}
- (IBAction)chatTapped{
  
  LGChatController *chatController = [LGChatController new];
  
  //chatController.opponentImage = [UIImage imageNamed:@"User"];
  chatController.title = @"Simple Chat";
  LGChatMessage *helloWorld = [[LGChatMessage alloc] initWithContent:@"Hello World!" sentByString:[LGChatMessage SentByUserString]];
  chatController.messages = @[helloWorld]; // Pass your messages here.
  chatController.delegate = self;
  [self.navigationController pushViewController:chatController animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  return 0;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  NotesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notesCell"];
  Request *request =self.selectedRequest;
  cell.authorLabel.text = request.requestDescription;
  cell.notesLabel.text = request.requestTime;
  return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  [self performSegueWithIdentifier:@"showRequestDetail" sender:self];
}

-(BOOL)hidesBottomBarWhenPushed
{
  return YES;
}

#pragma mark - LGChatControllerDelegate

- (void)chatController:(LGChatController *)chatController didAddNewMessage:(LGChatMessage *)message
{
  NSLog(@"Did Add Message: %@", message.content);
}

- (BOOL)shouldChatController:(LGChatController *)chatController addMessage:(LGChatMessage *)message
{
  /*
   This is implemented just for demonstration so the sent by is randomized.  This way, the full functionality can be demonstrated.
   */
  message.sentByString = arc4random_uniform(2) == 0 ? [LGChatMessage SentByOpponentString] : [LGChatMessage SentByUserString];
  return YES;
}

- (IBAction)callTapped:(UIBarButtonItem*)sender{
//  if ([sender.title isEqualToString:@"Call"]){
//    [sender setTitle:@"End Call"];
//  [[CingoSession sharedCingoSession]setDelegate:self];
//  [[CingoSession sharedCingoSession]dial];
//  }
//  else{
    [sender setTitle:@"End Call"];
    [self.subscriberView removeFromSuperview];
    [[CingoSession sharedCingoSession]endCall];
//  }
}

- (void)sessionCallInitiated{
  [[CingoSession sharedCingoSession]setDelegate:self];
  [[CingoSession sharedCingoSession]dial];
}
- (void)streamAvailableForRendering:(UIView *)subscriber{
  [subscriber setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
  [self.view addSubview:subscriber];
  [self.view sendSubviewToBack:subscriber];
  _subscriberView = subscriber;
}
@end
