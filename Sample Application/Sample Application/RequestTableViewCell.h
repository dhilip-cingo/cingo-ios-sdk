//
//  RequestTableViewCell.h
//  Sample Application
//
//  Created by Dhilip on 5/26/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestTableViewCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel *statusTextField, *timeTextField,*requestDescriptionTextField;
@end
