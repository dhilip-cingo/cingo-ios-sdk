//
//  RequestsViewController.h
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,weak)IBOutlet UITableView *requestsTableView;
- (void)showSignInViewController;
@end
