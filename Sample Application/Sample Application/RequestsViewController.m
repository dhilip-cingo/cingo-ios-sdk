//
//  RequestsViewController.m
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "RequestsViewController.h"
#import "RequestTableViewCell.h"
#import "Request.h"
#import "ObjectiveRecord.h"
#import "RequestDetailViewController.h"
@interface RequestsViewController ()
@property(nonatomic,strong)NSArray *requestsArray;
@property(nonatomic,weak)Request *selectedRequest;
@end

@implementation RequestsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  self.title = @"Requests";
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    [self performSegueWithIdentifier:@"SignInPresent" sender:self];
  });
  //[self.requestsTableView registerClass:[RequestTableViewCell class] forCellReuseIdentifier:@"requestTableCell"];
  
    // Do any additional setup after loading the view.
}

- (void)showSignInViewController{
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    [self performSegueWithIdentifier:@"SignInPresent" sender:self];
  });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
  [super viewWillAppear:animated];
  self.requestsArray = [Request all];
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    [self.requestsTableView reloadData];
  });
  
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
  if ([segue.identifier  isEqual: @"showRequestDetail"]){
  RequestDetailViewController *controller = [segue destinationViewController];
  controller.selectedRequest = self.selectedRequest;
  }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  return self.requestsArray.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  RequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"requestTableCell"];
  Request *request =[self.requestsArray objectAtIndex:indexPath.row];
  cell.requestDescriptionTextField.text = request.requestDescription;
  cell.timeTextField.text = request.requestTime;
  return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  self.selectedRequest = self.requestsArray[indexPath.row];
  [self performSegueWithIdentifier:@"showRequestDetail" sender:self];
}
@end
