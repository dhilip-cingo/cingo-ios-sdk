//
//  SettingsTableViewController.h
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewController : UITableViewController
@property(nonatomic,weak)IBOutlet UITextField *fullNameTextField,*phoneNumberTextField,*emailAddressTextField,*passwordTextField,*confirmPasswordTextField, *streetTextField,*zipcodeTextField,*cityTextField,*stateTextField;
- (IBAction)saveTapped:(id)sender;
- (IBAction)signOutTapped:(id)sender;
@end
