//
//  SettingsTableViewController.m
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "ObjectiveRecord.h"
#import "Customer.h"
#import <Cingo/Cingo.h>
#import "Utils.h"
#import "RequestsViewController.h"
@interface SettingsTableViewController ()
@property (nonatomic,strong)Customer *currentCustomer;
@end

@implementation SettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Sign Out"
                                                                  style:UIBarButtonItemStylePlain target:self  action:@selector(signOutTapped:)];
  [self.navigationItem setRightBarButtonItem:rightButton];
  self.title = @"Settings";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated{
  [super viewWillAppear:animated];
  Customer *currentCustomer = [Customer currentCustomer];
  _fullNameTextField.text = currentCustomer.fullName ;
  _phoneNumberTextField.text = currentCustomer.phoneNumber ;
  _emailAddressTextField.text =  currentCustomer.email ;
  _passwordTextField.text = currentCustomer.password ;
  _confirmPasswordTextField.text = @"";
  _streetTextField.text =  currentCustomer.street;
  _zipcodeTextField.text =  currentCustomer.zipcode;
  _cityTextField.text =  currentCustomer.city;
  _stateTextField.text = currentCustomer.state;
  self.currentCustomer = currentCustomer;
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveTapped:(id)sender{
  [self.view endEditing:true];
  CingoCustomer *customer = [[CingoCustomer alloc]init];
  [customer setEmail:_emailAddressTextField.text];
  [customer setLastName:_fullNameTextField.text];
  [customer setFirstName:_fullNameTextField.text];
  [customer setPassword:_passwordTextField.text];
  [customer setMobileNumber:_phoneNumberTextField.text];
  [customer setCity:_cityTextField.text];
  [customer setStreet:_streetTextField.text];
  [customer setZipcode:_zipcodeTextField.text];
  [customer setState:_stateTextField.text];
  [customer setCustomerId:self.currentCustomer.customerId];
  
  [[CingoSession sharedCingoSession]changeCustomerSettings:customer callback:^(CingoCustomer *responseObject, NSError *error) {
    if(!error){
      [self.currentCustomer setEmail:customer.email];
      [self.currentCustomer setFullName:customer.fullName];
      [self.currentCustomer setPassword:customer.password];
      [self.currentCustomer setPhoneNumber:customer.mobileNumber];
      [self.currentCustomer setState:customer.state];
      [self.currentCustomer setStreet:customer.street];
      [self.currentCustomer setZipcode:customer.zipcode];
      [self.currentCustomer setCity:customer.city];
      BOOL success = [self.currentCustomer save];
      [Utils showSuccessAlert:@"Details saved"];
      NSLog(@"Success %d",success);
    }
    else{
      [Utils showErrorAlert:@"Settings not saved. Please check the form"];
    }
  }];

 
  
}

- (IBAction)signOutTapped:(id)sender{
  
  UINavigationController *navigationController = self.tabBarController.viewControllers[0];
  [navigationController popToRootViewControllerAnimated:NO];
  RequestsViewController *reqController = navigationController.topViewController;
  [reqController showSignInViewController];
  [self.tabBarController setSelectedIndex:0];
}

@end
