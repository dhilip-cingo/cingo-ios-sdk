//
//  SignInViewController.h
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInViewController : UITableViewController
@property(nonatomic,weak)IBOutlet UITextField *emailAddressTextField,*passwordTextField;
- (IBAction)signUpTapped:(id)sender;
- (IBAction)signInTapped:(id)sender;
@end
