//
//  SignInViewController.m
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "SignInViewController.h"
#import <Cingo/Cingo.h>
#import "Utils.h"
#import "ObjectiveRecord.h"
#import "Customer.h"

@interface SignInViewController ()

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  self.title = @"Sign In";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
  
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
  [super viewWillAppear:animated];
  NSArray *customers = [Customer all];
  if ([customers count]>0){
    Customer *customer = [customers lastObject];
    self.emailAddressTextField.text = customer.email;
    self.passwordTextField.text = customer.password;
  }
}
- (IBAction)signUpTapped:(id)sender{
   [self performSegueWithIdentifier:@"SignUpShow" sender:self];
}
- (IBAction)signInTapped:(id)sender{
  [[CingoSession sharedCingoSession]signInCustomer:_emailAddressTextField.text password:_passwordTextField.text callback:^(CingoCustomer *customer, NSError *error) {
    
    if (!error){
      if (!error){
        Customer *cust = [Customer create];
        [cust setEmail:customer.email];
        [cust setFullName:customer.fullName];
        [cust setPassword:customer.password];
        [cust setPhoneNumber:customer.mobileNumber];
        [cust setCustomerId:customer.customerId];
        [cust save];
      }
    dispatch_async(dispatch_get_main_queue(), ^{
      [[CingoSession sharedCingoSession]startWebrtcSession];
      [self dismissViewControllerAnimated:YES completion:^{
   
   
      }];
    });
    }
    else{
      [Utils showErrorAlert:error.localizedDescription];
    }
    
  }];
  
  
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
