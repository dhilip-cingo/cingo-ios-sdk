//
//  SignupViewController.h
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupViewController : UITableViewController
@property(nonatomic,weak)IBOutlet UITextField *fullNameTextField,*phoneNumberTextField,*emailAddressTextField,*passwordTextField,*confirmPasswordTextField;
- (IBAction)signUpTapped:(id)sender;
@end
