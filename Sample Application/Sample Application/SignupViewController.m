//
//  SignupViewController.m
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "SignupViewController.h"
#import <Cingo/Cingo.h>
#import "Utils.h"
#import "Customer.h"
#import "ObjectiveRecord.h"
@interface SignupViewController ()

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  self.title = @"Sign Up";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signUpTapped:(id)sender{
  NSLog(@"%@ %@ %@ %@ %@",_fullNameTextField.text,_phoneNumberTextField.text,_emailAddressTextField.text,_passwordTextField.text,_confirmPasswordTextField.text);
  CingoCustomer *customer = [[CingoCustomer alloc]init];
  [customer setEmail:_emailAddressTextField.text];
  [customer setLastName:_fullNameTextField.text];
  [customer setFirstName:_fullNameTextField.text];
  [customer setPassword:_passwordTextField.text];
  [customer setMobileNumber:_phoneNumberTextField.text];
 [[CingoSession sharedCingoSession]signUpNewCustomer:customer callback:^(CingoCustomer *customer,NSError *error) {
   if (!error){
     Customer *cust = [Customer create];
     [cust setEmail:customer.email];
     [cust setFullName:customer.fullName];
     [cust setPassword:customer.password];
     [cust setPhoneNumber:customer.mobileNumber];
     [cust setCustomerId:customer.customerId];
     BOOL success = [cust save];
     NSLog(@"Save customer Success %d",success);
     [Utils showAlertView:@"" message:@"Registration Success" button1:nil button1Action:nil otherButton:@"Ok"];
     [self.navigationController popViewControllerAnimated:YES];
   }
   else{
     NSDictionary *userInfo = [error userInfo];
     NSString *cingoError = userInfo[@"error"];
     if ([[userInfo allKeys]containsObject:@"error"]){
       [Utils showAlertView:@"" message:cingoError button1:nil button1Action:nil otherButton:@"Ok"];
     }
     else{
       [Utils showAlertView:@"" message:error.localizedDescription button1:@"Ok" button1Action:nil otherButton:@"Ok"];
     }
   }
   
 }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
