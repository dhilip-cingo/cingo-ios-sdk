//
//  Utils.h
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Utils : NSObject
+ (void)showAlertView:(NSString*)title message:(NSString*)message button1:(NSString*)button1 button1Action:(void (^ __nullable)(UIAlertAction *action))button1Action otherButton:(NSString*)cancelButton;
+ (void)showSuccessAlert:(NSString*)message;
+ (void)showErrorAlert:(NSString*)message;
+ (UIImage*)imageForFile:(NSString*)fileName;
+ (void)writeImageToFile:(NSString*)fileName image:(UIImage*)image;
@end
