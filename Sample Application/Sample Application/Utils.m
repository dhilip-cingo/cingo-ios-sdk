//
//  Utils.m
//  Sample Application
//
//  Created by Dhilip on 5/25/16.
//  Copyright © 2016 Cingo. All rights reserved.
//

#import "Utils.h"
#import "SCLAlertView.h"
@implementation Utils
+ (void)showAlertView:(NSString*)title message:(NSString*)message button1:(NSString*)button1 button1Action:(void (^ __nullable)(UIAlertAction *action))button1Action otherButton:(NSString*)cancelButton{
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert title" message:@"Alert message" preferredStyle:UIAlertControllerStyleAlert];
  
  
  UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelButton style:UIAlertActionStyleDefault handler:nil];
  [alertController addAction:cancel];
  if(button1){
    UIAlertAction* customAction = [UIAlertAction actionWithTitle:button1 style:UIAlertActionStyleDefault handler:button1Action ];
    [alertController addAction:customAction];
    
  }
  
  
  UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
  dispatch_async(dispatch_get_main_queue(), ^{
    [topVC presentViewController:alertController animated:YES completion:nil];
  });
  
}

+ (void)showSuccessAlert:(NSString*)message{
  dispatch_async(dispatch_get_main_queue(), ^{
   UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
  SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
  [alert showSuccess:topVC title:@"Success" subTitle:message closeButtonTitle:nil duration:2.0f];
      });
}

+ (void)showErrorAlert:(NSString*)message{
  dispatch_async(dispatch_get_main_queue(), ^{
  UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
  SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
  [alert showError:topVC title:@"Error" subTitle:message closeButtonTitle:nil duration:2.0f];
  });
}

+ (void)writeImageToFile:(NSString*)fileName image:(UIImage*)image{
  NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
  NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",fileName]];
  NSData *imageData = UIImagePNGRepresentation(image);
  [imageData writeToFile:filePath atomically:YES];
}

+ (UIImage*)imageForFile:(NSString*)fileName{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",fileName]];
  return [UIImage imageWithContentsOfFile:filePath];
}
@end
